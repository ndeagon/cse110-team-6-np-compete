package com.telecommgroup.telecomm;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    protected EditText usernameName;
    protected EditText usernameEmail;
    protected EditText usernamePassword;
    protected Button registerButton;
    TextView emailaddress;
    TextView pword;
    MyDBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Initialize
        usernameName = (EditText)findViewById(R.id.nameRegisterEditText);
        usernameEmail = (EditText)findViewById(R.id.emailRegisterEditText);
        usernamePassword = (EditText)findViewById(R.id.passwordRegisterEditText);
        registerButton = (Button)findViewById(R.id.registerButton);
        //listen to register button click
         dbHandler = new MyDBHandler(this, null, null, 1);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //toast
                Toast.makeText(MainActivity.this, "Account Created", Toast.LENGTH_LONG).show();
            }


    });
    /*// Add member to database
    public void printDatabase(){
        String dbString = dbHandler.databaseToString();
        emailaddress.setText(dbString);
        pword.setText(dbString);
        email.setText("");
        pass.setText("");
    }
        */
}
}
