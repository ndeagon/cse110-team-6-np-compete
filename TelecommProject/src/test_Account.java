/* Filename: test_Account.java
 * Author: NP Compete
 * Description: Test program for Account.java
 * Date: 1/24/15
 */

import static org.junit.Assert.*;


public class test_Account
{
   public static void main(String[] args)
   {
      System.out.println("Testing Account.java ....");

      Account acc = new Account();

      acc.setFirstName("John");

      assertEquals(acc.getFirstName(), "John");

      acc.setLastName("Doe");

      assertEquals(acc.getLastName(), "Doe");


      System.out.println("Finished testing Account.java");
      return;
   }
}
