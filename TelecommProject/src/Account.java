/* Filename: Account.java
 * Author: NP Compete
 */
public class Account 
{
   //private Purchasable[] = {};
 
   private String firstName;
   private String lastName;
   private String password;
   private String email;

   public Account()
   {
   }

   public void setFirstName(String name)
   {
      firstName = name;
   }

   public void setLastName(String name)
   {
      lastName = name;
   }

   public void setPassword(String pass)
   {
      password = pass;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public String getFirstName()
   {
      return firstName;
   }

   public String getLastName()
   {
      return lastName;
   }

   public String getPassword()
   {
      return password;
   }

   public String getEmail()
   {
      return email;
   }
}
